from distutils.core import setup

readme = ""
try:
  with open("README.md", 'r') as f:
    readme = f.read()
except:
  pass

setup(
  name = "chemprop-why",
  packages = ["cpwhy"],
  version = "0.1.4",
  description = "An explainer for Chemprop based on LIME.",
  long_description = readme,
  long_description_content_type = "text/markdown",
  author = "Fabien BERNIER",
  author_email = "fabien.bernier@inria.fr",
  url = "https://gitlab.inria.fr/fbernier1/chemprop-why",
  keywords = [
    "explainer",
    "explanation",
    "visualization",
    "chemprop",
    "molecule",
    "model",
    "deep learning",
    "graph"
  ],
  classifiers = [],
)
